window.addEventListener('load', function () {
    document.querySelectorAll('form').forEach(function (form) {
        form.addEventListener('submit', function (e) {
            e.preventDefault();
            var data = {}

            build = function (name, value, parent) {
                let index = name.indexOf("_");
                if (name.indexOf("{0}") >= 0) {
                    return parent;
                }
                if (index < 0) {
                    if (value === 'true' || value === 'false') {
                        value = (value === 'true');
                    }
                    parent[name] = value;
                    return parent;
                }
                let key = name.substring(0, index);
                if (isEmpty(parent) && !isNaN(key)) {
                    parent = [];
                }
                let child = parent[key];
                if (isEmpty(child)) {
                    child = {};
                }
                parent[key] = build(name.substr(index + 1), value, child);
                return parent;
            };

            writeData = function (e) {
                var name = e.attributes['name'];
                // check if belongs in array
                if (name !== undefined && e.value && e.value !== '') {
                    data = build(name.value, e.value, data);
                }
            };

            for (let element of form.elements) {
                writeData(element);
            }
            if (form.method.toUpperCase() === "GET") {
                var query = "";
                if (Object.keys(data).length > 0) {
                    query = serialize(data);
                }
                window.location = form.action + query;
            } else {
                let xhr = new XMLHttpRequest();
                xhr.open(form.method, form.action);
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == XMLHttpRequest.DONE) {
                        if (xhr.status == 200) {
                            ajaxSuccess(JSON.parse(xhr.responseText), xhr);
                        } else {
                            ajaxFailure(JSON.parse(xhr.responseText));
                        }
                    }
                }
                xhr.send(JSON.stringify(data), xhr);
            }
        });
    });

    function serialize(obj) {
        let prefix = '?';
        let uri = '';

        for (let key of Object.keys(obj)) {
            uri = uri + prefix + key + '=' + obj[key];
            prefix = '&'
        }
        return uri;
    }

    function isEmpty(obj) {
        return !obj || Object.keys(obj).length === 0 && obj.constructor === Object;
    }

    function ajaxSuccess(data, xhr) {
        var location = data['redirect'] || xhr.getResponseHeader("Location")
        if (location !== null && location.length > 0) {
            window.location.replace(location);
        } else if (!data['status']) {
            for (var key in data) {
                document.querySelectorAll('*[name=' + key + ']').forEach(function (input) {
                    input.value = data[key];
                });
            }
        } else {
            window.location.reload();
        }
    }

    function ajaxFailure(error, xhr) {
        if (error !== undefined) {
            var details = error['details'];
            if (Array.isArray(details)) {
                for (var e of details) {
                    var msg = e['error']
                    if (e['name'] !== undefined) {
                        msg = e['name'] + ": " + msg;
                    }
                    newMessage(msg, "danger", 2);
                }
            } else {
                newMessage(details, "danger", 2);
            }
        }
        return;
    }

    function newMessage(data, type, duration) {
        document.querySelectorAll('.message').forEach(function (el) {
            el.insertAdjacentHTML('beforeend', '<div class="js-message row col-xs-offset-4 col-xs-4 message-box ' + type + '-box">' + data + '<a class="close">×</a></div>')
        });
        document.querySelectorAll('.js-message').forEach(function (el) {
            setTimeout(function () {
                el.parentElement.removeChild(el);
            }, duration * 1000);
            el.addEventListener('click', function (e) {
                el.parentElement.removeChild(el);
            });
        });
    }
});

function ignoreEvent(e) {
    e.stopPropagation();
    e.preventDefault();
}

function openDialog(id) {
    document.getElementById(id).className = 'dialog';
    document.querySelector('body').className = 'dialog-open';
}

function closeDialog(id, event) {
    if (event && event.target.id === id) {
        closeDialog(id);
    } else if (!event) {
        document.getElementById(id).className = 'dialog hidden';
        document.querySelector('body').className = '';
    }
}