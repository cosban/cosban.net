$.fn.raptorize = function() {

    var img = '/static/img/raptor.png';

    var audio = new Audio();
    audio.src = '/static/audio/raptor-sound.ogg';

    //Raptor Vars
    var raptorImageMarkup = '<img id="Raptor" style="display: none" src="' + img + '" />'
    var raptorAudioMarkup = '<audio id="RaptorShriek" preload="auto"><source src="' + audio.src + '" /></audio>';
    var locked = false;

    //Append Raptor and Style
    $('body').append(raptorImageMarkup);
    $('body').append(raptorAudioMarkup);
    var raptor = $('#Raptor').css({
        "position": "fixed",
        "bottom": "-700px",
        "right": "0",
        "display": "block"
    })

    // Animating Code
    function init() {
        console.log("called");
        locked = true;

        //Sound Hilarity
        function playSound() {
            document.getElementById('RaptorShriek').play();
        }
        playSound();

        // Movement Hilarity
        raptor.animate({ "bottom": "+=600" }, 400);
        raptor.animate({ "right": "+=2000" }, 1200);
        // Reset animation
        raptor.animate({ "bottom": "-=600" }, 1);
        raptor.animate({ "right": "-=2000" }, 1);
        locked = false;
    }

    var keys = [],
        konami = "38,38,40,40,37,39,37,39,66,65";
    $(document).keyup(function(event) {
        if (!locked) {
            keys.push(event.keyCode);
            if (keys.toString().indexOf(konami) >= 0) {
                init();
                keys = [];
            }
        }
    });
}