(function (document, window) {
    let commands = [];
    let command = "";
    let sounds = {};
    let commandIndex = -1;
    let scroll = 0;
    let outputLocked = false;
    let inputLocked = false;
    let obfuscateInput = false;
    let output = null;
    let input = null;
    let PS1 = null;
    let cursorPosition = 0;
    let cursorInterval = null;
    let socket = null;
    document.onreadystatechange = function () {
        if (document.readyState === 'complete') {
            initialize();
        }
    }

    function initialize() {
        output = document.getElementById('output');
        input = document.getElementById('input');
        initializeSocket();

        document.addEventListener("keydown", event => keydown(event));
        document.addEventListener("keyup", event => keyup(event));
    }

    function initializeSocket() {
        console.log("initializing socket");
        socket = getSocket();
        let initialized = false;
        let reconnectWait = 1000;
        socket.onopen = function (event) {
            reconnectWait = 1000;
        }
        socket.onmessage = function (event) {
            let events = event.data.split("\n");
            for (data of events) {
                let action = JSON.parse(data);
                handleAction(action);
                if (!initialized) {
                    resetInput();
                    initialized = true;
                }
            }
        }
        socket.onclose = function (event) {
            console.log("reconnecting in " + reconnectWait / 1000 + " seconds");
            setTimeout(() => {
                socket = new WebSocket("wss://cosban.net/uos");
            }, reconnectWait);
            if (reconnectWait < 300000) {
                reconnectWait = reconnectWait * 2;
            }
        }
    }

    function resetInput() {
        modifyCursorPosition("", "");
    }

    function initializeBlinking() {
        let cursor = document.getElementById('cursor');
        if (cursorInterval != null) {
            clearInterval(cursorInterval);
        }
        cursorInterval = setInterval(function () {
            let opacity = Number(cursor.style.opacity);
            cursor.style.opacity = (opacity + 1) % 2;
        }, 600);
    }

    function keydown(event) {
        event.preventDefault();
        if (inputLocked) {
            return;
        }
        switch (event.key) {
            case "Enter": {
                onEnter();
                return;
            }
            default: {
                if (event.key.length === 1) {
                    insertCharacter(event.key, cursorPosition);
                } else {
                    initializeBlinking();
                }
            }
        }
    }

    function keyup(event) {
        if (inputLocked) {
            return;
        }
        event.preventDefault();
        switch (event.key) {
            case "Backspace": {
                event.preventDefault();
                onRemoveCharacter(true);
                return;
            }
            case "Delete": {
                onRemoveCharacter(false);
                return;
            }
            case "PageUp": {
                onPageButton(-23);
                return;
            }
            case "PageDown": {
                onPageButton(23);
                return;
            }
            case "End": {
                onChangeCursor(command.length);
                return;
            }
            case "Home": {
                onChangeCursor(1);
                return;
            }
            case "ArrowLeft": {
                onChangeCursor(cursorPosition - 1);
                return;
            }
            case "ArrowRight": {
                onChangeCursor(cursorPosition + 1);
                return;
            }
            case "ArrowUp": {
                if (commands.length > 0) {
                    commandIndex = (commandIndex + 1) % commands.length;
                    setInput(commands[commandIndex]);
                }
                return;
            }
            case "ArrowDown": {
                if (commands.length > 0) {
                    if (commandIndex <= 0) {
                        setInput('');
                        initializeBlinking();
                    } else {
                        commandIndex = (commandIndex + 1) % commands.length;
                        setInput(commands[commandIndex]);
                    }
                }
                return;
            }
            default: { }
        }
    }

    function onEnter() {
        let line = input.textContent;
        if (commands.length === 500) {
            history.pop();
        }
        commands.unshift(command);
        commandIndex = -1;
        cursorPosition = 0;
        if (command.length > 0) {
            output.insertAdjacentHTML('beforeend', `<p>${line}</p>`);
            socket.send(command);
        }
        command = "";
        toggleInputEnabled()
    }

    function onRemoveCharacter(prefix) {
        removeCharacter(cursorPosition, prefix);
    }

    function onPageButton(amount) {
        scroll = setView(scroll + amount);
    }

    function onChangeCursor(position) {
        cursorPosition = setCursor(position)
        initializeBlinking();
    }

    var setView = function (height) {
        let output = document.getElementById('output');
        let input = document.getElementById('input');
        let terminal = document.getElementById('terminal');
        var max = output.height + input.height - 600;
        if (max < 0) {
            max = 0;
        }
        if (height < 0) {
            height = 0;
        } else if (height > max) {
            height = max;
        }
        terminal.scroll({
            top: height,
            behavior: 'smooth'
        });
        return height;
    }

    var checkFlag = function (flag) {
        if (flag === false) {
            console.log("flag was false");
            setTimeout(checkFlag(flag), 1000);
        } else {
            console.log("flag was true");
            return false;
        }
    }

    var setCursor = function (pos) {
        if (pos < 0) {
            pos = 0;
        }
        if (pos > command.length) {
            pos = command.length;
        }
        var prefix = command.substring(0, pos);
        var suffix = command.substr(pos);
        modifyCursorPosition(prefix, suffix);
    }

    var toggleInputEnabled = function () {
        if (inputLocked) {
            inputLocked = false;
            setInput('')
        } else {
            inputLocked = true;
            input.innerHTML = '';
        }
    }

    var autoType = function (text) {
        var output = output.childNodes[output.childNodes.length - 1];
        outputLocked = true;
        var textloop = function (text) {
            setTimeout(function () {
                output.append(text[0])
                if (text.length > 1) {
                    textloop(text.substr(1));
                } else {
                    outputLocked = false;
                }
            }, 30);
        }
        textloop(text);
    }

    var setInput = function (text) {
        var textloop = function (text) {
            setTimeout(function () {
                insertCharacter(text[0], cursorPosition);
                if (text.length > 1) {
                    textloop(text.substr(1));
                }
                initializeBlinking();
            }, 5);
        }
        modifyCursorPosition("", "");
        if (text.length > 0) {
            textloop(text);
        }
    }

    var handleAction = function (action) {
        if (!outputLocked) {
            if (action.ps1 !== undefined) {
                PS1 = action.ps1;
            }
            handleCommands(action);
            scroll = setView(scroll + 23);
        }
        if (inputLocked) {
            toggleInputEnabled();
        }
    }

    var handleCommands = function (action) {
        if (action.commands !== undefined && action.commands !== null && action.commands.length > 0) {
            for (cmd of action.commands) {
                switch (cmd.action) {
                    case 0: {
                        output.insertAdjacentHTML('beforeend', `<p>${cmd.value}</p>`);
                        break;
                    }
                    case 1: {
                        sounds[cmd.value] = new Audio(cmd.value);
                        break;
                    }
                    case 2: {
                        sounds[cmd.value].play();
                        break;
                    }
                    case 3: {
                        output.insertAdjacentHTML('beforeend', `<p><img src="${cmd.value}"></img></p>`);
                        break;
                    }
                    case 4: {
                        clearOutput();
                        break;
                    }
                    case 5: {
                        obfuscateInput = !obfuscateInput;
                        break;
                    }
                    case 6: {
                        commands = [];
                        break;
                    }
                    default:
                        console.log("default" + cmd)
                }
            }
        }
    }

    var clearOutput = function () {
        output.innerHTML = '';
        resetInput();
        history = [];
        outputLocked = true;
        setTimeout(function () {
            outputLocked = false;
        }, 50);
    }

    var insertCharacter = function (c, pos) {
        var prefix = command.substring(0, pos);
        var suffix = command.substr(pos);
        prefix += c;
        modifyCursorPosition(prefix, suffix);
    }

    var removeCharacter = function (pos, before) {
        let prefix = null;
        let suffix = null;
        if (before && pos > 0) {
            prefix = command.substring(0, pos - 1);
            suffix = command.substr(pos);
            pos = pos - 1;
        } else if (pos < command.length + 1) {
            prefix = command.substring(0, pos);
            suffix = command.substr(pos + 1);
        }
        modifyCursorPosition(prefix, suffix);
    }

    var modifyCursorPosition = function (prefix, suffix) {
        cursorPosition = prefix.length;
        command = prefix + suffix;
        if (obfuscateInput) {
            prefix = new Array(prefix.length + 1).join('*');
            suffix = new Array(suffix.length + 1).join('*');
        }
        input.innerHTML = `${PS1}${prefix}<span id="cursor"></span>${suffix}`;
        initializeBlinking();
    }
}(document, window));
